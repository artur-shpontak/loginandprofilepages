import firebase from 'firebase/app';
require('firebase/auth');
require('firebase/firestore');
require('firebase/firebase-storage');

import { saveUserToDeviceStorage } from './deviceStorage';

export async function firebaseLogIn(email, password, setError) {
  await firebase.auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => {
      saveUserToDeviceStorage([email, password]);
    })
    .catch((error) => {
      if (error.code === 'auth/invalid-email') {
        setError({
          isError: true,
          errorMessage: 'That email address is invalid!',
          errorType: 'email'
        });
      }

      if (error.code === 'auth/user-not-found') {
        setError({
          isError: true,
          errorMessage: 'There is no user corresponding to this email.',
          errorType: 'email'
        });
      }

      if (error.code === 'auth/wrong-password') {
        setError({
          isError: true,
          errorMessage: 'Wrong password! Try again please.',
          errorType: 'password'
        });
      }

      if (error.code === 'auth/too-many-requests') {
        setError({
          isError: true,
          errorMessage: 'Too many requests! Try again later.',
          errorType: 'password'
        });
      }

      throw new Error(error);
    });
}

export function firebaseSignUp(email, password, setError, navigation) {
  firebase.auth()
    .createUserWithEmailAndPassword(email, password)
    .then(() => {
      saveUserToDeviceStorage([email, password]);
      //TODO find the better way to solve problem with sign up errors
      navigation.navigate('Profile');
    })
    .catch((error) => {
      if (error.code === 'auth/email-already-in-use') {
        setError({
          isError: true,
          errorMessage: 'The email address is already in use.',
          errorType: 'email'
        });
      }

      if (error.code === 'auth/invalid-email') {
        setError({
          isError: true,
          errorMessage: 'The email adress is badly formatted.',
          errorType: 'email'
        });
      }

      console.error(error);
    });
}

// TODO fix(doesn't work correctly)
export function loadUserFromServer(setUser, setImage, userId) {
  const db = firebase.firestore();
  const docRef = db.collection('users').doc(userId);

  docRef.get()
    .then((doc) => {
      if (doc.exists) {
        setUser(doc.data().user);
        setImage(doc.data().imageUrl)
      }
    })
    .catch((error) => {
      console.log("Error getting document:", error);
    });
}

export function signOut() {
  firebase.auth().signOut()
    .catch((error) => {
      console.log(error);
    })
}
