import AsyncStorage from '@react-native-async-storage/async-storage';

export async function saveUserToDeviceStorage(user) {
  try {
    await AsyncStorage.setItem('user', JSON.stringify(user));
  } catch (err) {
    alert(err);
  }
}

export async function loadUserFromDeviceStorage(setUser) {
  try {
    const user = await AsyncStorage.getItem('user');

    if (user !== null) {
      setUser(JSON.parse(user));
    }
  } catch (error) {
    alert(error);
  }
}

export async function removeUserFromDeviceStorage() {
  try {
    await AsyncStorage.removeItem('user');
  } catch (error) {
    alert(error);
  }
}
