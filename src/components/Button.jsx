import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export const Button = ({ handlePress, title }) => (
  <TouchableOpacity onPress={handlePress}>
    <Text style={styles.button}>
      {title}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginTop: 30,
    paddingVertical: 10,
    textAlign: 'center',
    backgroundColor: '#2f5fda',
    borderRadius: 15,
    color: '#fff'
  }
});
