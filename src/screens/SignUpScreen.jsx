import React, { useCallback, useEffect, useState } from 'react';
import { Image, StyleSheet, Text, TextInput, View } from 'react-native';

import Icon from '@expo/vector-icons/AntDesign';
import { firebaseSignUp } from '../api/firebase';
import { Button } from '../components/Button';

export const SignUpScreen = ({ navigation }) => {
  const [isVisiblePassword, setIsVisiblePassword] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [error, setError] = useState({
    isError: false,
    errorMessage: '',
    errorType: ''
  });

  const handlePasswordVisibility = useCallback(() => {
    setIsVisiblePassword(prevValue => !prevValue);
  }, [isVisiblePassword]);

  const signUp = useCallback(() => {
    if (!email) {
      setError({
        isError: true,
        errorMessage: 'Enter email first.',
        errorType: 'email'
      });

      return;
    }

    if (password.trim() !== confirmPassword.trim()) {
      setError({
        isError: true,
        errorMessage: 'Password confirmation doesn\'t match.',
        errorType: 'password'
      });

      return;
    }

    if (password.length < 6) {
      setError({
        isError: true,
        errorMessage: 'Password needs to be at least 6 characters.',
        errorType: 'password'
      });

      return;
    }

    firebaseSignUp(email, password, setError, navigation);
  }, [
    email,
    password,
    confirmPassword
  ]);

  useEffect(() => {
    setError({
      isError: false
    });
  }, [
    email,
    password,
    confirmPassword
  ]);

  return (
    <View style={styles.container}>
      <View style={styles.mainInformation}>
        <Image
          style={styles.logo}
          source={{
            uri: 'https://uploads-ssl.webflow.com/5f24ef7c947241f45dedd5ab/603d10ef3f9bc70ce5a871e8_logo-social.png'
          }}
        />
        <Text style={styles.heading}>
          Company name
        </Text>
        <Text style={styles.description}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin venenatis pulvinar tortor, non viverra
          tellus tincidunt et. Suspendisse vitae rutrum metus, eget hendrerit nunc.
        </Text>
      </View>

      <View style={styles.loginContainer}>
        <View style={styles.inputContainer}>
          <Icon
            name="mail"
            color="#8b9dc3"
            size={24}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            onChangeText={text => setEmail(text.trim())}
          />
        </View>
        {error.isError
          && error.errorType === 'email'
          && <Text style={styles.errorMessage}>{error.errorMessage}</Text>
        }
        <View style={styles.inputContainer}>
          <Icon
            name="lock"
            color="#8b9dc3"
            size={24}
          />
          <TextInput
            secureTextEntry={isVisiblePassword}
            style={styles.input}
            placeholder="Password"
            onChangeText={text => setPassword(text)}
          />
          <Icon
            name={isVisiblePassword ? "eyeo" : 'eye'}
            color="#8b9dc3"
            size={24}
            onPress={handlePasswordVisibility}
          />
        </View>
        <View style={styles.inputContainer}>
          <Icon
            name="lock"
            color="#8b9dc3"
            size={24}
          />
          <TextInput
            secureTextEntry={isVisiblePassword}
            style={styles.input}
            placeholder="Confirm password"
            onChangeText={text => setConfirmPassword(text)}
          />
        </View>
        {error.isError
          && error.errorType === 'password'
          && <Text style={styles.errorMessage}>{error.errorMessage}</Text>
        }
        <Button handlePress={signUp} title="Sign Up" />
        <Text style={styles.signupQuestion}>
          Already have an account?
        </Text>
        <Text style={styles.link} onPress={() => navigation.navigate('Log In')}>
          Log In
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 40,
    backgroundColor: '#fff'
  },
  mainInformation: {
    flex: 0.5,
    justifyContent: 'center',
    height: '100%'
  },
  loginContainer: {
    flex: 0.5
  },
  logo: {
    width: '100%',
    height: '50%'
  },
  heading: {
    textAlign: 'center',
    paddingBottom: 10,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#444'
  },
  description: {
    textAlign: 'center',
    fontSize: 15,
    color: '#888'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginVertical: 5,
    borderWidth: 2,
    borderRadius: 15,
    borderColor: '#8b9dc3'
  },
  input: {
    paddingHorizontal: 10,
    width: '85%'
  },
  signupQuestion: {
    marginTop: 20,
    textAlign: 'center',
    color: '#8b9dc3'
  },
  link: {
    textAlign: 'center',
    color: '#2f5fda'
  },
  errorMessage: {
    color: '#f00'
  }
});
