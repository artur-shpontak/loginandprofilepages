import React, { useState, useCallback, useEffect } from 'react';
import { Image, View, Text, ScrollView, StyleSheet} from 'react-native';

import firebase from 'firebase/app';
require('firebase/firestore');
require('firebase/firebase-storage');

import { Button } from '../components/Button';
import { removeUserFromDeviceStorage } from '../api/deviceStorage';

if (firebase.apps.length === 0) {
  firebase.initializeApp({
    apiKey: "AIzaSyC-n1EE9YeoImkzw7jg1jqYGThfsR5RsVs",
    authDomain: "test-rn-login.firebaseapp.com",
    projectId: "test-rn-login",
    storageBucket: 'test-rn-login.appspot.com'
  });
};

const db = firebase.firestore();

export const ProfileScreen = ({ navigation }) => {
  const [user, setUser] = useState({});
  const [image, setImage] = useState();
  const [userId, setUserId] = useState(firebase.auth().currentUser.uid);
  const [email, setEmail] = useState(firebase.auth().currentUser.email);

  const getAge = () => {
    return Math.round(
      (new Date().getTime() - new Date(user.birth).getTime()) / (24 * 3600 * 365.25 * 1000)
    );
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setUserId(firebase.auth().currentUser.uid);
      setEmail(firebase.auth().currentUser.email);
      getData();
    });

    return unsubscribe;
  }, []);

  const getData = useCallback(() => {
    const docRef = db.collection('users').doc(userId);

    docRef.get()
      .then((doc) => {
        if (doc.exists) {
          setUser(doc.data().user);
          setImage(doc.data().imageUrl)
        }
      })
      .catch((error) => {
        console.log("Error getting document:", error);
      });
  })

  const logout = useCallback(() => {
    firebase.auth().signOut()
      .then(() => {
        navigation.navigate('Log In');
        removeUserFromDeviceStorage();
      })
      .catch((error) => {
        console.log(error);
      });
  });

  const navigateToEditProfile = useCallback(() => {
    navigation.navigate('Edit Profile');
  }, []);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.profile}>
        <Image
          style={styles.photo}
          source={image
            ? { uri: image }
            : require('./images/avatardefault_92824.png')
          }
        />
        <View style={{flexDirection: 'row'}}>
          {!!user.firstName && <Text style={styles.name}>{user.firstName} </Text>}
          {!!user.lastName && <Text style={styles.name}>{user.lastName}</Text>}
        </View>
        {!!user.about && <Text style={styles.about}>{user.about}</Text>}
        {!!user.birth && !!user.showBirth &&
          <Text style={styles.info}>
            Date of birth:&nbsp;
            {new Date(user.birth).toDateString().slice(4)}
            {`(${user.gender === 'Male' ? 'He' : 'She'} is ${getAge()} y.o.)`}
          </Text>
        }
        {!!email && <Text style={styles.info}>Email: {email}</Text>}
        {!!user.phone && <Text style={styles.info}>Phone: {user.phone}</Text>}
      </View>
      <View>
        <Button handlePress={navigateToEditProfile} title="Edit Profile" />
        <Text style={styles.logOut} onPress={logout}>
          Log Out
        </Text>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 40,
    backgroundColor:'white',
  },
  photo: {
    height: 200,
    width: 200,
    borderRadius: 150
  },
  profile: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  name: {
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 20
  },
  about: {
    textAlign:'center',
    fontSize: 18,
    marginBottom: 30
  },
  image: {
    width: 200,
    height: 200,
    borderRadius: 150
  },
  info: {
    alignSelf: 'flex-start',
    marginBottom: 10
  },
  confirm: {
    width: '100%',
    marginTop: 30,
    paddingVertical: 10,
    textAlign: 'center',
    backgroundColor: '#2f5fda',
    borderRadius: 15,
    marginBottom: 10
  },
  confirmText: {
    textAlign: 'center',
    color: '#fff'
  },
  logOut: {
    textAlign: 'center',
    color: '#2f5fda',
    marginTop: 10,
    marginBottom: 50
  }
})
