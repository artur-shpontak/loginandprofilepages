import React, { useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Swing } from 'react-native-animated-spinkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from 'firebase/app';
require('firebase/auth');

export const Loading = ({navigation}) => {
  useEffect(() => {
    const getUserFromDeviceStorage = async () => {
      try {
        const user = await AsyncStorage.getItem('user');
        const [ email, password ] = JSON.parse(user);
        firebase.auth()
          .signInWithEmailAndPassword(email, password)
          .then(() => navigation.navigate('Profile'))
      } catch (err) {
        navigation.navigate('Log In')
      }
    };
    getUserFromDeviceStorage()
  }, [])

  return (
  <View style={styles.container}>
    <Swing size={120} color="white" />
    <Text style={styles.text}>Loading...</Text>
  </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2f5fda'
  },
  text: {
    color: 'white',
    marginTop: 40,
    fontSize: 25,
    fontWeight: '400'
  }
})
