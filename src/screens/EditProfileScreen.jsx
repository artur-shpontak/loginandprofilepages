import React, {useState, useEffect, useCallback} from 'react';
import { Image, View, TextInput, Text, TouchableOpacity, ScrollView, Switch, StyleSheet} from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as ImagePicker from 'expo-image-picker';
import { Picker } from '@react-native-picker/picker';

import firebase from 'firebase/app';
require('firebase/firestore');
require('firebase/firebase-storage');

import { Button } from '../components/Button';
import { removeUserFromDeviceStorage } from '../api/deviceStorage';

if (firebase.apps.length === 0) {
  firebase.initializeApp({
    apiKey: "AIzaSyC-n1EE9YeoImkzw7jg1jqYGThfsR5RsVs",
    authDomain: "test-rn-login.firebaseapp.com",
    projectId: "test-rn-login",
    storageBucket: 'test-rn-login.appspot.com'
  });
};

const db = firebase.firestore();

export const EditProfileScreen = ({ navigation }) => {
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [image, setImage] = useState(null);
  const [isEnabled, setIsEnabled] = useState(true);
  const [user, setUser] = useState({
    firstName:'',
    lastName:'',
    showBirth: true,
    gender: '',
    phone: '',
    about: ''
  });

  useEffect(() => {
    const docRef = db.collection('users').doc(userId);

    docRef.get()
      .then((doc) => {
        if (doc.exists) {
          setImage(doc.data().imageUrl);
          setUser(doc.data().user);
          setIsEnabled(doc.data().user.showBirth)
        } else {
          db.collection("users").doc(userId).set({user, imageUrl: image});
        }
      })
      .catch((error) => {
        console.log("Error getting document:", error);
      });
  }, []);

  const userDB = firebase.auth().currentUser;
  const userId = firebase.auth().currentUser.uid;
  const email = firebase.auth().currentUser.email;

  const save = useCallback(() => {
    db.collection("users").doc(userId).update({user})
      .then(() => {
        navigation.navigate('Profile');
      })
      .catch((error) => {
        alert(error);
      });
  }, [user, image])

  const showDatepicker = useCallback(() => {
    setShow(true);
  }, [show]);

  const uploadImage = useCallback (async (projectSavePic)  => {
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', projectSavePic, true);
      xhr.send(null);
    });

    const ref = firebase
      .storage()
      .ref()
      .child(`images/${userId}/photo.jpg`)
    const snapshot = await ref.put(blob)
      .then((response) => {
        response.ref.getDownloadURL().then((downloadURL) => {
          db.collection("users").doc(userId).update({imageUrl:downloadURL})})})
      .catch(err => console.log(err));
    blob.close();
    return await snapshot.ref.getDownloadURL();
  }, [image])

  const onChange = useCallback((event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
    setUser((currentUser) => ({
      ...currentUser,
      birth: currentDate.toDateString().slice(4)
    }));
  }, [show]);

  const pickImage = useCallback(async () => {
    (async () => {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    })();

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      uploadImage(result.uri);
    }
  }, []);

  const toggleSwitch = useCallback(() => {
    setIsEnabled(previousState => !previousState);
    setUser(currentUser => ({
      ...currentUser,
      showBirth: !isEnabled
    }));
  }, [isEnabled]);

  const logout = useCallback(() => {
    firebase.auth().signOut()
      .then(() => {
        navigation.navigate('Log In');
        removeUserFromDeviceStorage();
      })
      .catch((error) => {
        console.log(error);
      });
  });

  const confirm = useCallback(()=> {
    save(user, image);
  }, [user, image]);

  const deleteAccount = useCallback(() => {
    deleteUserFromDeviceStorage()
    firebase
      .storage()
      .ref()
      .child(`images/${userId}/photo.jpg`)
      .delete()
    userDB.delete().then(function() {
      db.collection("users").doc(userId).delete()
    }).catch(function(error) {
      console.log(error);
    });
    navigation.navigate('Log In');
  }, [])

  return (
    <ScrollView style={styles.container}>
      <View style={styles.headerBlock}>
        <Image
          style={styles.photo}
          source={image
            ? { uri: image }
            : require('./images/avatardefault_92824.png')
          }
        />
        <TouchableOpacity style={styles.addPhotoIcon} onPress={pickImage}>
          <Ionicons
            name="add-circle"
            size={35}
            color="#8b9dc3"
          />
        </TouchableOpacity>
        <Text style={styles.title}>
          {email}
        </Text>
      </View>

      <View>
        <View style={styles.input}>
          <TextInput
            defaultValue={user.firstName}
            style={styles.inputInner}
            placeholder="First name"
            placeholderTextColor='#8b9dc3'
            onChangeText={firstName => setUser(currentUser => ({...currentUser, firstName: firstName.trim()}))}
          />
        </View>
        <View style={styles.input}>
          <TextInput
            defaultValue={user.lastName}
            style={styles.inputInner}
            placeholder="Last name"
            placeholderTextColor='#8b9dc3'
            onChangeText={lastName => setUser(currentUser => ({...currentUser, lastName: lastName.trim()}))}
          />
        </View>
        <View style={styles.input}>
          <Picker
            selectedValue={user.gender}
            style={styles.picker}
            onValueChange={gender => setUser(currentUser => ({...currentUser, gender}))}>
            <Picker.Item label="Gender not selected" value="Gender not selected" />
            <Picker.Item label="Male" value="Male" />
            <Picker.Item label="Female" value="Female" />
          </Picker>
        </View>
        <View style={styles.input} >
          <TextInput
            defaultValue={user.phone}
            style={styles.inputInner}
            keyboardType='phone-pad'
            placeholder="Phone number"
            placeholderTextColor='#8b9dc3'
            onChangeText={phone => setUser(currentUser => ({...currentUser, phone}))}
          />
        </View>
        <TouchableOpacity onPress={showDatepicker} style={styles.input}>
          <Text style={styles.birth}>
            Date of Birth:&nbsp;
            <Text style={{color: 'black'}}>
              {user.birth
                ? new Date(user.birth).toDateString().slice(4)
                : 'Not selected'
              }
            </Text>
          </Text>
        </TouchableOpacity>
        {show && (
          <DateTimePicker
            value={date}
            mode="date"
            display="calendar"
            onChange={onChange}
            maximumDate={new Date()}
            minimumDate={new Date(1900, 0, 1)}
          />
        )}
        <View style={styles.switch}>
          <Text>Show your age</Text>
          <Switch
            trackColor={{ false: '#767577', true: '#81b0ff' }}
            thumbColor={isEnabled ? '#2f5fda' : '#f4f3f4'}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
        <View style={styles.input}>
          <TextInput
            defaultValue={user.about}
            style={styles.inputInner}
            placeholder="Write something about yourself..."
            multiline={true}
            numberOfLines={3}
            maxLength={300}
            placeholderTextColor='#8b9dc3'
            onChangeText={about => setUser((currentUser) => ({
              ...currentUser,
              about: about.trim()
            }))}
          />
        </View>
        <Button handlePress={confirm} title="Confirm changes" />
        <Text style={styles.logOut} onPress={logout}>
          Log Out
        </Text>
        <Text style={styles.span}>or</Text>
        <Text style={styles.delete} onPress={deleteAccount}>
          Delete account
        </Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 40,
    backgroundColor:'white'
  },
  title: {
    paddingBottom: 10,
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#444'
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 15,
    borderColor: '#8b9dc3',
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 10
  },
  headerBlock: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  photo: {
    width: 200,
    height: 200,
    borderRadius: 150
  },
  addPhotoIcon: {
    position: 'relative',
    bottom: 30,
    left: 50,
    backgroundColor:'white',
    borderRadius: 50,
    paddingHorizontal: 2
  },
  picker: {
    height: 28,
    width: '100%',
    borderWidth: 0
  },
  inputInner: {
    paddingHorizontal: 10,
    width: '100%'
  },
  birth: {
    paddingHorizontal: 10,
    paddingVertical: 4,
    width: '100%',
    color:"#8b9dc3"
  },
  logOut: {
    textAlign: 'center',
    color: '#2f5fda',
    marginTop: 10
  },
  delete: {
    textAlign: 'center',
    color: 'red',
    marginBottom: 50
  },
  switch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  span: {
    textAlign: 'center',
    marginVertical: 8
  }
});
