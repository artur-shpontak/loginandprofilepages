import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { LogInScreen } from './src/screens/LogInScreen';
import { SignUpScreen } from './src/screens/SignUpScreen';
import { ProfileScreen } from './src/screens/ProfileScreen';
import { EditProfileScreen } from './src/screens/EditProfileScreen';
import firebase from 'firebase/app';
import { Loading } from './src/screens/Loading';

const Stack = createStackNavigator();

const firebaseConfig = {
  apiKey: "AIzaSyC-n1EE9YeoImkzw7jg1jqYGThfsR5RsVs",
  authDomain: "test-rn-login.firebaseapp.com",
  projectId: "test-rn-login",
  storageBucket: "test-rn-login.appspot.com",
  messagingSenderId: "1088148657406",
  appId: "1:1088148657406:web:d29cb1a16b97814fa581af",
  measurementId: "G-PHLVV82GLX"
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
};

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Loading'
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Loading" component={Loading} />
        <Stack.Screen name="Log In" component={LogInScreen} />
        <Stack.Screen name="Sign Up" component={SignUpScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="Edit Profile" component={EditProfileScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
